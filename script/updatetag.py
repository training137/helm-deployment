import sys, getopt, ruamel.yaml, os
os.chdir(os.path.dirname(os.path.abspath(__file__)))

FILE_URL=''
def modify_yaml(SERVICE,TAG):
   if SERVICE=='' or TAG=='':
      print("service and tag must have value")
      print("python3 update-tag.py -i <service> -t <tag>")
      sys.exit()
   FILE_URL='../'+SERVICE+'/values.yaml'
   print(FILE_URL)
   yaml = ruamel.yaml.YAML()
   with open(FILE_URL) as f:
       list_doc = yaml.load(f)
   list_doc['image']['tag'] = TAG

   with open(FILE_URL,"w") as fi:
       yaml.dump(list_doc,fi)
       print("successfully")
def main(argv):
   SERVICE = ''
   TAG = ''
   try:
      opts, args = getopt.getopt(argv,":s:t:",["service","tag"])
      if not opts:
         print("python3 update-tag.py -s <service> -t <tag>")
         sys.exit()
   except getopt.GetoptError as e:
      print(e)
      sys.exit()
   for opt, arg in opts:
      if opt in ("-s","--service"):
         SERVICE = arg
      elif opt in ("-t","--tag"):
         TAG = arg
   modify_yaml(SERVICE,TAG)

if __name__ == '__main__':
   main(sys.argv[1:])
